init: config docker-build
check: lint test-full
test-full: init test-unit test-e2e

find-node-ip:
	docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $$(docker-compose ps -q ledius-fund)

config:
	cp .env.example .env

test-unit:
	npm run test

test-e2e:
	docker-compose run --rm app npm run test:e2e

lint:
	npm run lint

clear:
	docker-compose down --remove-orphans

docker-up:
	docker-compose up -d

docker-stop:
	docker-compose down

up:
	docker-compose up

docker-build:
	docker-compose build

.SILENT: find-node-ip
