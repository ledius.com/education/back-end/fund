const digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] as const;
type Digit = typeof digits[number];
type Month =
  | '1'
  | '2'
  | '3'
  | '4'
  | '5'
  | '6'
  | '7'
  | '8'
  | '9'
  | '10'
  | '11'
  | '12';
type Year = `20${Digit}${Digit}`;
