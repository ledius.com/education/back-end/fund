import { Module } from '@nestjs/common';
import CoinGecko from 'coingecko-api';

@Module({
  providers: [
    {
      provide: CoinGecko,
      useFactory: () => {
        return new CoinGecko();
      },
    },
  ],
  exports: [CoinGecko],
})
export class CoinGeckoModule {}
