import { ICommand } from '@nestjs/cqrs';
import { Type } from 'class-transformer';

export class SummaryCommand implements ICommand {
  @Type(() => Date)
  public dates: Date[];
}
