/**

 1. получить список первого числа месяца [01-01-2021, 01-02-2021, 01-03-2021]
 2. получить tuple цен на начало месяца и конец месяца [ [10, 11], [11, 21], [21, 10] ]
 3. преобразовать в процент, и fund series

 */
import CoinGecko from 'coingecko-api';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { SummaryCommand } from './summary-command';
import { endOfMonth, format, isAfter, startOfMonth } from 'date-fns';

export interface FundSeries {
  profit: number;
  date: `${Month}-${Year}`;
}

@CommandHandler(SummaryCommand)
export class SummaryHandler
  implements ICommandHandler<SummaryCommand, FundSeries[]>
{
  constructor(private readonly coinGecko: CoinGecko) {}

  public async execute(command: SummaryCommand): Promise<FundSeries[]> {
    const prices = await Promise.all(
      command.dates
        .filter((date) => isAfter(startOfMonth(new Date()), date))
        .map(async (date) => {
          const [start, end] = [startOfMonth(date), endOfMonth(date)];
          const [{ data: startData }, { data: endData }] = await Promise.all([
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            this.coinGecko.coins.fetchHistory('bitcoin', {
              date: format(start, 'dd-MM-yyyy'),
            }),
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            this.coinGecko.coins.fetchHistory('bitcoin', {
              date: format(end, 'dd-MM-yyyy'),
            }),
          ]);
          return [
            Math.floor(startData.market_data.current_price.usd),
            Math.floor(endData.market_data.current_price.usd),
            format(start, 'MM-yyyy'),
          ] as [number, number, `${Month}-${Year}`];
        }),
    );
    return prices.map(([start, end, date]) => {
      return {
        date,
        profit: ((end - start) / start) * 100,
      } as unknown as FundSeries;
    });
  }
}
