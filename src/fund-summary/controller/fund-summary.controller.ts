import {
  Body,
  CacheInterceptor,
  Controller,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { SummaryCommand } from '../handler/summary-command';
import { FundSummaryCacheInterceptor } from '../interceptor/fund-summary-cache.interceptor';

@Controller({
  version: '1',
  path: 'fund-summary',
})
export class FundSummaryController {
  constructor(private readonly commandBus: CommandBus) {}

  @Post()
  @UseInterceptors(FundSummaryCacheInterceptor)
  public async fundSummary(@Body() command: SummaryCommand): Promise<unknown> {
    return this.commandBus.execute(command);
  }
}
