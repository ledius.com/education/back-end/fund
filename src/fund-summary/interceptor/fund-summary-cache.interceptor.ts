import { CacheInterceptor, ExecutionContext, Injectable } from '@nestjs/common';

@Injectable()
export class FundSummaryCacheInterceptor extends CacheInterceptor {
  protected trackBy(context: ExecutionContext): string | undefined {
    const request = context.switchToHttp().getRequest();
    return Buffer.from(request.url + JSON.stringify(request.body)).toString(
      'base64',
    );
  }
}
