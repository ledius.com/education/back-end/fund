import { CacheModule, Module } from '@nestjs/common';
import { CoinGeckoModule } from '../coin-gecko/coin-gecko.module';
import { SummaryHandler } from './handler/summary-handler';
import { CqrsModule } from '@nestjs/cqrs';
import { FundSummaryController } from './controller/fund-summary.controller';

@Module({
  imports: [CoinGeckoModule, CqrsModule, CacheModule.register({})],
  providers: [SummaryHandler],
  controllers: [FundSummaryController],
})
export class FundSummaryModule {}
